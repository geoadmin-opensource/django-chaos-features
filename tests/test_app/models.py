# coding: utf-8
from django.contrib.gis.db import models


class DummyGeoModel(models.Model):

    name = models.CharField(
        max_length=128
    )

    geometry = models.PointField()
