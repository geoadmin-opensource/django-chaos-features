# coding: utf-8
from django.test import (
    SimpleTestCase,
    TestCase
)
from django.contrib.gis.geos import Point, Polygon
from django.contrib.contenttypes.models import ContentType
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from chaos_features.models import Dataset
from chaos_features.factory import (
    DatasetRelatedFactory,
    GeoJSONSerializerFactory
)
from tests.test_app.models import DummyGeoModel


class DatasetRelatedFactoryTestCase(SimpleTestCase):

    def test_init(self):

        drf = DatasetRelatedFactory()
        self.assertIsNotNone(drf)
        self.assertEqual(drf.class_prefix, '')
        self.assertEqual(drf.class_suffix, '')

    def test_get_class_name(self):

        ds = Dataset(name='foo')
        drf = DatasetRelatedFactory()
        self.assertEqual(drf.get_class_name(ds), 'foo')

    def test_create_should_throw_not_implemented(self):

        with self.assertRaises(NotImplementedError):
            drf = DatasetRelatedFactory()
            drf.create('foo')


class GeoJSONSerializerFactoryTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(GeoJSONSerializerFactoryTestCase, cls).setUpClass()

    def test_init(self):
        geojson_serializer = GeoJSONSerializerFactory()
        self.assertIsNotNone(geojson_serializer)

    def test_create_meta_should_return_class(self):
        ct = ContentType.objects.get_for_model(DummyGeoModel)
        ds = Dataset(content_type=ct)
        factory = GeoJSONSerializerFactory()
        meta = factory.create_meta(ds)
        self.assertIsNotNone(meta)
        self.assertTrue(issubclass(meta, object))
        self.assertEqual(meta.model, DummyGeoModel)
        self.assertEqual(meta.geo_field, ds.geometry_field)

    def test_create_should_return_serializer(self):
        ct = ContentType.objects.get_for_model(DummyGeoModel)
        ds = Dataset(content_type=ct)
        factory = GeoJSONSerializerFactory()
        serializer = factory.create(ds)
        self.assertIsNotNone(serializer)
        self.assertTrue(issubclass(serializer, GeoFeatureModelSerializer))
        self.assertTrue(hasattr(serializer, 'Meta'))

    def test_create_without_dataset_should_throw(self):
        factory = GeoJSONSerializerFactory()
        with self.assertRaises(ValueError):
            factory.create(None)

    def test_serializer_should_return_a_feature(self):
        ct = ContentType.objects.get_for_model(DummyGeoModel)
        dataset = Dataset(content_type=ct)
        factory = GeoJSONSerializerFactory()
        serializer_class = factory.create(dataset)
        feature = dataset.get_model().objects.create(
            name='foo',
            geometry=Point(0, 0)
        )
        feature = dataset.features.get_queryset().get(name='foo')
        result = serializer_class(feature).data
        self.assertIsNotNone(result)
        self.assertIn('type', result)
        self.assertEqual(result['type'], 'Feature')
        self.assertIn('geometry', result)
        self.assertIn('properties', result)

    def test_serializer_should_return_a_collection(self):
        ct = ContentType.objects.get_for_model(DummyGeoModel)
        dataset = Dataset(content_type=ct)
        factory = GeoJSONSerializerFactory()
        serializer_class = factory.create(dataset)
        dataset.get_model().objects.create(
            name='foo',
            geometry=Point(0, 0)
        )
        dataset.get_model().objects.create(
            name='bar',
            geometry=Point(0, 0)
        )
        features = dataset.features.get_queryset()
        result = serializer_class(features, many=True).data
        self.assertIsNotNone(result)
        self.assertIn('type', result)
        self.assertIn('features', result)
        self.assertEqual(result['type'], 'FeatureCollection')
