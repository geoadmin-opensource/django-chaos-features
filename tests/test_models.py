# coding: utf-8
from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from chaos_features.features import FeatureManager
from chaos_features.models import Dataset
from tests.test_app.models import DummyGeoModel


class DatasetTestCase(TestCase):

    def test_init_without_content_type(self):
        ds = Dataset()
        self.assertIsNone(ds.features)

    def test_init_with_content_type(self):
        ct = ContentType.objects.get_for_model(DummyGeoModel)
        ds = Dataset(content_type=ct)
        self.assertIsNotNone(ds.features)
        self.assertTrue(isinstance(ds.features, FeatureManager))
        self.assertEqual(ds.features.dataset, ds)
