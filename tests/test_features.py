# coding: utf-8
from django.test import SimpleTestCase
from django.db.models.query import QuerySet
from chaos_features.models import Dataset
from chaos_features.features import FeatureManager
from tests.test_app.models import DummyGeoModel


class FeatureManagerTestCase(SimpleTestCase):

    def test_init_should_work(self):
        feature_man = FeatureManager(Dataset(), DummyGeoModel)
        self.assertIsNotNone(feature_man)

    def test_init_without_dataset_fails(self):
        with self.assertRaises(ValueError):
            FeatureManager(None, DummyGeoModel)

    def test_init_without_model_fails(self):
        with self.assertRaises(ValueError):
            FeatureManager(Dataset(), None)

    def test_get_queryset_returns_queryset_instance(self):
        feature_man = FeatureManager(Dataset(), DummyGeoModel)
        queryset = feature_man.get_queryset()
        self.assertIsNotNone(queryset)
        self.assertTrue(isinstance(queryset, QuerySet))
