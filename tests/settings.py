# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

import django

DEBUG = True
USE_TZ = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "!_ioq!if1!uz1baf7$*ir)2_bqp46@-iq8mq=&u%u&5-3qi_7s"

DATABASES = {
    "default": {
        'ENGINE': 'django.contrib.gis.db.backends.spatialite',
        "NAME": ":memory:",
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django.contrib.gis",
    "chaos_features",
    "tests.test_app",
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

SPATIALITE_LIBRARY_PATH = 'mod_spatialite'
