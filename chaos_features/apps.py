# -*- coding: utf-8
from django.apps import AppConfig


class ChaosFeaturesConfig(AppConfig):
    name = 'chaos_features'
