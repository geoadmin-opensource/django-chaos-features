# coding: utf-8
from django.utils.translation import ugettext as _


BEHAVIOR_APPEND = _('APPEND')
BEHAVIOR_OVERWRITE = _('OVERWRITE')
BEHAVIOR_NO_IMPORT = _('NO_IMPORT')

BEHAVIOR_CHOICES = (
    (BEHAVIOR_APPEND, _('Append')),
    (BEHAVIOR_OVERWRITE, _('Overwrite')),
    (BEHAVIOR_NO_IMPORT, _('No Import')),
)
