# coding: utf-8
import logging
from fiona.crs import to_string
from django.db import transaction
from django.db.models import F, Value, CharField
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.gdal import SpatialReference
from .signals import (
    pre_import,
    post_import,
)

logger = logging.getLogger(__name__)


class FeatureManager(object):

    """
    Special manager attached to the
    dataset INSTANCE.

    Allows us to query features, etc.
    """

    def __init__(self, dataset, model):
        if not dataset:
            raise ValueError('dataset must not be null')
        if not model:
            raise ValueError('model must not be null')
        self.dataset = dataset
        self.model = model

    def export_features(self, **kwargs):
        raise NotImplementedError

    def import_features(self, **kwargs):
        io = FeatureIO(
            self.model,
            self.dataset
        )
        io.import_features(**kwargs)

    def get_queryset(self):
        return self.model.objects.all()\
            .annotate(label=F(self.dataset.label_field))\
            .annotate(dataset=Value(self.dataset.name, CharField()))


class FieldMapper(object):

    """
    Generates and validates field maps
    """

    def __init__(self, model, dataset, reader):
        self.model = model
        self.dataset = dataset
        self.reader = reader

    def validate(self, field_map):
        pass

    def build(self):
        pass


class FeatureIO(object):

    """
    Class responsible to make
    all things importable/exportable
    """

    def __init__(self, dataset, model, **kwargs):
        self.dataset = dataset
        self.model = model

    def get_reader(self, **kwargs):
        """
        constructs a fiona reader and returns it
        """
        pass

    def import_feature(self, reader, field_map, feature, **kwargs):
        """
        field_map specifies a dict in the following format.
        The keys represent fields in the external field,
        while the values represent the fields for our model.
        """
        logger.info('Importing feature')
        if not field_map:
            raise ValueError('field_map must not be null')
        if not feature:
            logger.warning('feature is null. skipping.')

        instance = self.model()

        for out_field, in_field in field_map.items():
            value = feature.properties.get(out_field)
            setattr(instance, in_field, value)

        geometry = self.transform_geometry(feature, **kwargs)
        setattr(instance, self.dataset.geometry_field, geometry)
        instance.save()
        return instance

    def transform_geometry(self, feature, **kwargs):
        external_srid = kwargs.get('external_srid')
        internal_srid = kwargs.get('internal_srid')
        if not external_srid:
            external_srid = internal_srid

        initial_geometry = GEOSGeometry(feature.get('geometry'), srid=external_srid)

        if external_srid != internal_srid:
            return initial_geometry.transform(internal_srid, clone=True)
        return initial_geometry.clone()

    @transaction.atomic
    def import_features(self, **kwargs):
        try:
            from .choices import (
                BEHAVIOR_NO_IMPORT,
                BEHAVIOR_OVERRIDE,
            )
            logger.info('Importing data for model %s', self.model)
            options = dict(kwargs)
            if self.dataset.behavior == BEHAVIOR_NO_IMPORT:
                return
            with self.get_reader(**kwargs) as reader:

                options['external_srid'] = SpatialReference(to_string(reader.crs)).srid
                options['internal_srid'] = self.dataset.get_srid()

                pre_import.send_robust(
                    sender=self.__class__,
                    model=self.model,
                    dataset=self.dataset,
                    options=options
                )

                try:
                    last_feature = self.model.objects.all().order_by('-id')[0]
                    last_id = last_feature.pk
                except:
                    last_id = -1

                field_mapper = FieldMapper(self.model, self.dataset, reader)
                field_map = options.get('field_map')
                if field_map:
                    field_mapper.validate(field_map)
                else:
                    field_map = field_mapper.build()

                for feature in reader:
                    self.import_feature(reader, field_map, feature, **kwargs)
                if self.dataset.behavior == BEHAVIOR_OVERRIDE:
                    logger.info('BEHAVIOR is OVERRIDE. Deleting previous features')
                    self.model.objects.filter(id__lte=last_id).delete()

                post_import.send_robust(
                    sender=self.__class__,
                    model=self.model,
                    dataset=self.dataset,
                    options=options
                )

                logger.info('import_features finished successfully.')
        except:
            logger.error('something bad happened while trying to import features', exc_info=True)
