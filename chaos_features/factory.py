# coding: utf-8
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer


class DatasetRelatedFactory(object):

    class_prefix = ''
    class_suffix = ''

    def get_class_name(self, dataset):
        return '{}{}{}'.format(
            self.class_prefix,
            dataset.name,
            self.class_suffix
        )

    def create(self, dataset, **kwargs):
        raise NotImplementedError


class GeoJSONSerializerFactory(DatasetRelatedFactory):

    class_suffix = 'Serializer'

    def create_meta(self, dataset, **kwargs):
        model = dataset.get_model()
        if not model:
            raise ValueError('No model available.')
        fields = '__all__'
        return type('Meta', (object, ), {
            'model': model,
            'fields': fields,
            'geo_field': dataset.geometry_field,
        })

    def create(self, dataset, **kwargs):
        if not dataset:
            raise ValueError('dataset cannot be null')
        meta = self.create_meta(dataset, **kwargs)
        return type(
            self.get_class_name(dataset),
            (GeoFeatureModelSerializer,),
            {
                'label': serializers.CharField(read_only=True),
                'dataset': serializers.CharField(read_only=True),
                'Meta': meta
            }
        )
