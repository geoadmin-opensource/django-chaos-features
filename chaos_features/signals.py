# coding: utf-8
import django.dispatch


pre_import = django.dispatch.Signal(providing_args=['model', 'dataset', 'options'])  # noqa
post_import = django.dispatch.Signal(providing_args=['model', 'dataset', 'options'])  # noqa
