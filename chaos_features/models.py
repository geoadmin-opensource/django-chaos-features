# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
from django.contrib.contenttypes.models import ContentType
from common.models import (
    DateCreatedMixIn,
    DateUpdatedMixIn,
    CreatedByMixIn
)
from .features import FeatureManager
from .choices import (
    BEHAVIOR_CHOICES,
    BEHAVIOR_APPEND,
)


class UUIDPrimaryMixIn(models.Model):

    id = models.UUIDField(
        primary_key=True,
        verbose_name=_('ID')
    )

    class Meta:
        abstract = True


class Group(UUIDPrimaryMixIn,
            CreatedByMixIn,
            DateCreatedMixIn,
            DateUpdatedMixIn):

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=128
    )

    description = models.TextField(
        verbose_name=_('Description'),
        null=True
    )

    def __str__(self):
        return self.name


class Dataset(UUIDPrimaryMixIn,
              CreatedByMixIn,
              DateCreatedMixIn,
              DateUpdatedMixIn):

    @staticmethod
    def __new__(cls, *args, **kwargs):
        instance = super(Dataset, cls).__new__(cls, *args, **kwargs)
        ct = kwargs.get('content_type')
        if ct:
            model = ct.model_class()
            instance.features = FeatureManager(instance, model)
        else:
            instance.features = None
        return instance

    content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL
    )

    group = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL
    )

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=128,
    )

    alias = models.CharField(
        verbose_name=_('Alias'),
        max_length=256,
        null=True
    )

    description = models.TextField(
        verbose_name=_('Description'),
        null=True
    )

    behavior = models.CharField(
        verbose_name=_('Import Behavior'),
        max_length=32,
        default=BEHAVIOR_APPEND,
        choices=BEHAVIOR_CHOICES
    )

    id_field = models.CharField(
        verbose_name=_('ID Field'),
        max_length=64,
        default='id'
    )

    label_field = models.CharField(
        verbose_name=_('Label Field'),
        max_length=64,
        default='name'
    )

    geometry_field = models.CharField(
        verbose_name=_('Geometry Field'),
        max_length=64,
        default='geometry'
    )

    def get_srid(self):
        try:
            model = self.get_model()
            field = model._meta.get_field(self.geometry_field)
            return field.srid
        except:
            return None

    def get_model(self):
        """
        Returns the content-type model
        """
        if not self.content_type:
            return None
        return self.content_type.model_class()

    def __str__(self):
        if self.alias:
            return self.alias
        return self.name

    class Meta:

        get_latest_by = 'date_created'
        ordering = ('name',)
