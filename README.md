# django-chaos-features
django-chaos-features


### Ideally

```python

class Brigde(models.Model):

    attr_1 = models.CharField()

    class Dataset(chaos_features.Dataset):
        # dataset info
```

or

```
class BridgeDataset(chaos_features.Dataset):

    data

    class Meta:
        model = Bridge
```

At runtime, this should identify all the available datasets
and register those to the database