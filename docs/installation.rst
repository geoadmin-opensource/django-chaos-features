============
Installation
============

At the command line::

    $ easy_install django-chaos-features

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-chaos-features
    $ pip install django-chaos-features
