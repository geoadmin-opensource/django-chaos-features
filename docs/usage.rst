=====
Usage
=====

To use Django Chaos Features in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'chaos_features.apps.ChaosFeaturesConfig',
        ...
    )

Add Django Chaos Features's URL patterns:

.. code-block:: python

    from chaos_features import urls as chaos_features_urls


    urlpatterns = [
        ...
        url(r'^', include(chaos_features_urls)),
        ...
    ]
